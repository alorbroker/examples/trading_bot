/* здесь хранятся функции для получения статусов заявок */

let request = require("request");

let {
  track,
  API_WARP_URL,
  DEFAULT_POST_HEADERS
} = require('./parameters'); 

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

function OrdersStatus() {
}

OrdersStatus.prototype.getOrderStatusById = async function(options_ordersInfo) { 
  await utilities.sleep(700);
  return new Promise(function(resolve, reject) {
    request(options_ordersInfo, function (error, response, body) {
      
      if (error) {
        console.error(error); 
        console.error("При получении статуса существующей заявки получили ошибку. Опять вылетели по таймауту?"); 
        orderStatus.httpCode = 408;
        resolve(orderStatus);
        throw new Error(error);
      }
      if (response.statusCode !== 200) {
        console.log(response.statusCode + (body));
        console.log(options_ordersInfo.details + " последний запрос заявки/стопзаявки");
        console.log(options_ordersInfo.url);
        let orderStatus = {}//здесь body приходит как string
        orderStatus.httpCode = response.statusCode;
        resolve(orderStatus);
        console.error(error);
      } else {
        let orderStatus = JSON.parse(body); // здесь body приходит как string
        orderStatus.httpCode = response.statusCode;
        resolve(orderStatus);
      }
    });
  });
} 

OrdersStatus.prototype.obtainStatusMarketOrderBuyAsync = async function(orderNumber,jwt) { 
  const url = `${API_WARP_URL}/clients/GAME/D39004/orders/${orderNumber.toString()}`; 

  const newOrdersInfoOptions = {
    method: 'GET',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url,
    details :  `MarketOrderBuy ${orderNumber.toString()}`
  };

  track.lastDeal.orderMarket = await this.getOrderStatusById(newOrdersInfoOptions);
  
  if (track.lastDeal.orderMarket.httpCode !== 200) { 
    console.log("При получении статуса заявки вернулся код отличный от 200");
    track.lastDeal.orderMarket = await this.getOrderStatusById(newOrdersInfoOptions); // почему-то иногда не 200 а 500.
  }
  return track.lastDeal.orderMarket;
}

OrdersStatus.prototype.obtainStatusLimitOrderSellAsync = async function(orderNumber,jwt) { 
  const url = `${API_WARP_URL}/clients/GAME/D39004/orders/${orderNumber.toString()}`; 

  const newOrdersInfoOptions = {
    method: 'GET',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url,
    details :  `LimitOrderSell ${orderNumber.toString()}`
  };
  
  track.lastDeal.orderLimit = await this.getOrderStatusById(newOrdersInfoOptions);
  
  if (track.lastDeal.orderLimit.httpCode !== 200) { 
    console.log("При получении статуса заявки вернулся код отличный от 200");
    track.lastDeal.orderLimit = await this.getOrderStatusById(newOrdersInfoOptions);// почему-то иногда не 200 а 500.
  }
  return track.lastDeal.orderLimit;
}

OrdersStatus.prototype.obtainStatusStopLossOrderSellAsync = async function(orderNumber,jwt) { 
  const url = `${API_WARP_URL}/clients/GAME/D39004/stoporders/${orderNumber.toString()}`; 

  const newStopOrdersInfoOptions = {
    method: 'GET',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url,
    details :  `StopLossOrderSell ${orderNumber.toString()}`
  };
  
  track.lastDeal.orderStopLoss = await this.getOrderStatusById(newStopOrdersInfoOptions);
  
  if (track.lastDeal.orderStopLoss.httpCode !== 200) { 
    console.log("При получении статуса заявки вернулся код отличный от 200");
    track.lastDeal.orderStopLoss = await this.getOrderStatusById(newStopOrdersInfoOptions);// почему-то иногда не 200 а 500.
  }
  return track.lastDeal.orderStopLoss;
}

OrdersStatus.prototype.obtainStatusMarketOrderSellAsync = async function(orderNumber,jwt) { /* done */
  const url = `${API_WARP_URL}/clients/GAME/D39004/orders/${orderNumber.toString()}`; 

  const newOrdersInfoOptions = {
    method: 'GET',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url,
    details :  `MarketOrderSell ${orderNumber.toString()}`
  };

  track.lastDeal.orderMarket = await this.getOrderStatusById(newOrdersInfoOptions);
  if (track.lastDeal.orderMarket.httpCode !== 200) { 
    console.log("При получении статуса заявки вернулся код отличный от 200");
    track.lastDeal.orderMarket = await this.getOrderStatusById(newOrdersInfoOptions);// почему-то иногда не 200 а 500.
  }
  return track.lastDeal.orderMarket;
}

OrdersStatus.prototype.obtainStatusLimitOrderBuyAsync = async function(orderNumber,jwt) { /* done */
  const url = `${API_WARP_URL}/clients/GAME/D39004/orders/${orderNumber.toString()}`; 

  const newOrdersInfoOptions = {
    method: 'GET',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url,
    details :  `LimitOrderBuy ${orderNumber.toString()}`
  };

  track.lastDeal.orderLimit = await this.getOrderStatusById(newOrdersInfoOptions);

  if (track.lastDeal.orderLimit.httpCode !== 200) { 
    console.log("При получении статуса заявки вернулся код отличный от 200");
    track.lastDeal.orderLimit = await this.getOrderStatusById(newOrdersInfoOptions);// почему-то иногда не 200 а 500.
  }
  return track.lastDeal.orderLimit;
}

OrdersStatus.prototype.obtainStatusStopLossOrderBuyAsync = async function(orderNumber,jwt) { /* done */
  const url = `${API_WARP_URL}/clients/GAME/D39004/stoporders/${orderNumber.toString()}`; 

  const newStopOrdersInfoOptions = {
    method: 'GET',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url,
    details :  `StopLossOrderBuy ${orderNumber.toString()}`
  };

  track.lastDeal.orderStopLoss = await this.getOrderStatusById(newStopOrdersInfoOptions);

  if (track.lastDeal.orderStopLoss.httpCode !== 200) { 
    console.log("При получении статуса заявки вернулся код отличный от 200");
    track.lastDeal.orderStopLoss = await this.getOrderStatusById(newStopOrdersInfoOptions); 
  }
  return track.lastDeal.orderStopLoss;
}

module.exports = {
  // getOrderStatusById, 
  // obtainStatusMarketOrderBuyAsync, 
  // obtainStatusLimitOrderSellAsync, 
  // obtainStatusStopLossOrderSellAsync, 
  // obtainStatusMarketOrderSellAsync, 
  // obtainStatusLimitOrderBuyAsync, 
  // obtainStatusStopLossOrderBuyAsync
  OrdersStatus
}

