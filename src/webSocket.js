/* здесь описаны функции для создания и обработки веб сокет подписок */

const WebSocket = require('ws');

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

async function subscribeToWebSocket(track) {
  const guidBars = `${utilities.new_uuid()}`;
  const guidPositions = `${utilities.new_uuid()}`;
  const barTimeFrom = track.startingTime - (parseInt((new Date(1000 * track.startingTime)).toString().substring(16,18),10) - 8 ) * 60 * 60;
  
  let WSInitialMessageBars = {
    "opcode":"BarsGetAndSubscribe",
    "code":`${track.trade_parameters.ticker}`,
    "token":`${track.jwt}`,
    "tf":60, 
    "from":barTimeFrom,
    "exchange":"GAME",
    "format":"TV",
    "delayed":false,
    "guid":guidBars
  }

  let WSInitialMessagePositions = {
    "opcode":"PositionsGetAndSubscribe",
    "portfolio":"D39004",
    "token":`${track.jwt}`,
    "exchange":"GAME",
    "format":"TV",
    "guid":guidPositions
  }
  
  const ws = new WebSocket('wss://apidev.alor.ru/ws');

  track.test = true;
  let parsedMessage;
  let previousBarsMessage = null;
  track.wsHistory = [];
  
  if (track.jwt != null && track.jwt != undefined) {
        
    ws.on('open', async function open() {
      console.log('WebSocket подписка оформлена. ');
      // console.warn('трек.токен = ', track.jwt);
      track.isWsOpen = true;
      ws.send(JSON.stringify(WSInitialMessageBars)); 
      ws.send(JSON.stringify(WSInitialMessagePositions)); 
          /* 
            когда приходит сообщение вс, нужно проверить его гуид. нам нужны свечки.
            когда приходит сообщение, оно может либо быть самым первым, либо быть следующим.
            если сообщение первое, то надо запомнить положение его таймфрейма(условно время)
            если время совпадает со временем предыдущего сообщения, то надо запомнить сообщение 
            если это новый таймфрейм то надо взять предыдущее сообщение и положить его в историю свечей
            всего 31500 секунд за торговый день, и 525 минут. можно хранить и весь день.
          */
      ws.on('message', function incoming(message) {
        
        parsedMessage = JSON.parse(message);
        if (parsedMessage.requestGuid === guidBars && parsedMessage.message === 'Handled successfully') {
          console.log('ws Handled successfully : bars      with guid ' + parsedMessage.requestGuid);
        }
        
        if (parsedMessage.guid === guidBars) {
          if (previousBarsMessage === null) { 
            previousBarsMessage = parsedMessage;
          } else  { // первый таймфрейм
            
            if (parsedMessage.data.time === previousBarsMessage.data.time) {
              previousBarsMessage = parsedMessage;
            } else {
              track.wsHistory.push(previousBarsMessage.data); // сунуть в историю
              previousBarsMessage = parsedMessage;              
            }
          }
        }

        if (parsedMessage.requestGuid === guidPositions && parsedMessage.message === 'Handled successfully') {
          console.log('ws Handled successfully : positions with guid ' + parsedMessage.requestGuid);
        }
        
        if (parsedMessage.guid === guidPositions && parsedMessage.data.symbol === track.trade_parameters.ticker){
          track.positions = {};
          track.positions.qty = parsedMessage.data.qty;
        }
      });
      
      let currentServerTime = await utilities.callTime(track.jwt);
      if(((new Date(1000 * (currentServerTime))).toString().substring(0,3) !== 'Sat') && ((new Date(1000 * (currentServerTime))).toString().substring(0,3) !== 'Sun')){
        
        await utilities.sleep(1000 * (9 * 60 * 60 - (track.startingTime - barTimeFrom))); 
        
        /*
          это ожидание выполняется при открытии ws соединения: нужно задержаться 
          внутри функции в течение примерно 9 часов = 540 минут поскольку ws мы закроем 
          внутри этой же функции по прошествии этого времени.
        */

        let currentServerTime = await utilities.callTime(track.jwt);
        
        if( (parseInt((new Date(1000 * currentServerTime)).toString().substring(16,18)) <= 18) && (parseInt((new Date(1000 * currentServerTime)).toString().substring(16,18)) >= 9) ){
          await utilities.sleep(1000 * 60 * (60)); 
        }
      }

      /* 
        525 минут это один торговый день 
        поскольку робот спит в период с примерно 1930 до 0830 и значит не может инициировать подписку в это время
        то вне зависимости от времени начала работы  запаса в 60+60 минут 
        хватит для того чтобы подписка гарантированно не завершалась посредине торгового дня.
        самое позднее когда закроется вебсокет - 03:59:59
      */
      
      // await sleep(1000 * 60 * (10)); // малая задержка для отладки. вроде всё работает
      ws.terminate();
      track.wsHistory = [];
      track.isWsOpen = false;
      console.log('Робот закрыл WebSocket соединение, так как уже поздно. Робот откроет его заново с утра, если он продолжит работать.');
    });  
    
    ws.on('close', function close() {
      console.log('WebSocket соединение закрыто. Подписка остановлена. ');
      track.isWsOpen = false;
    });
  } else {
    console.log('WebSocket подписка не оформлена, так как не удалось получить токен ');
  }
  return;
}

module.exports = {
  subscribeToWebSocket  
}