
/* здесь описаны функции для расчетов цен */
let {
  trade_parameters  
} = require('./parameters'); 

// let {
//   Utilities
// } = require('./utilities');

// const utilities = new Utilities();

function PriceCalculation() {
}

PriceCalculation.prototype.movingAverageLong = function (localHistory) { /* done */
  let l = localHistory.length;
  let summ = 0.0;
  let summexp = 0.0;
  let timeDistance = 0.0;
  let timeframe = 1;

  for (let i = l - 1; i >= 0; i--) {
    timeDistance = (l - i - 1) * timeframe; //насколько текущий(в рамках цикла по i) таймфрейм отдален от самого нового
    summexp += Math.exp(-trade_parameters.exponentialDamper * timeDistance);
    summ += Math.exp(-trade_parameters.exponentialDamper * timeDistance) * localHistory[i].close;
    
    if (l - 1 - i > trade_parameters.longMALength - 1) {
      return summ / summexp;
    }
  } 
  return summ / summexp;
}

PriceCalculation.prototype.movingAverageShort = function(localHistory) { /* done */
  let l = localHistory.length;
  let summ = 0.0;
  let summexp = 0.0;
  let timeDistance = 0.0;
  let timeframe = 1;
  
  for (let i = l - 1; i >= 0; i--) {
    timeDistance = (l - i - 1) * timeframe; //насколько текущий(в рамках цикла по i) таймфрейм отдален от самого нового
    summexp += Math.exp(-trade_parameters.exponentialDamper * timeDistance);
    summ += Math.exp(-trade_parameters.exponentialDamper * timeDistance) * localHistory[i].close;
    
    if (l - 1 - i > trade_parameters.shortMALength - 1) {
      return summ / summexp;
    }
  } 
  return summ / summexp;
}

PriceCalculation.prototype.calculateBuyPricesForLimitOrder = async function(lastDeal_orderMarket_price) { /* done */
  let targetBuyPrice = lastDeal_orderMarket_price * (100 - trade_parameters.brokerComissionPercents - trade_parameters.toWinPercents) / (100 + trade_parameters.brokerComissionPercents)
  console.log('Продали за ' + lastDeal_orderMarket_price + ' выкупаем за ' + targetBuyPrice)
  return targetBuyPrice;
}

PriceCalculation.prototype.calculateBuyPricesForStopOrder = async function(lastDeal_orderMarket_price) { /* done */
  let targetTriggerBuyPrice = lastDeal_orderMarket_price * (100 - trade_parameters.brokerComissionPercents + trade_parameters.readyToLoosePercents) / (100 + trade_parameters.brokerComissionPercents)
  console.log('Продали за ' + lastDeal_orderMarket_price + ' выкупаем за ' + targetTriggerBuyPrice + ' при росте цены. SL')
  return targetTriggerBuyPrice ;
}

PriceCalculation.prototype.calculateSellPricesForLimitOrder = async function(lastDeal_orderMarket_price) { /* done */
  let targetSellPrice = lastDeal_orderMarket_price * (100 + trade_parameters.brokerComissionPercents + trade_parameters.toWinPercents) / (100 - trade_parameters.brokerComissionPercents)
  console.log('Купили за ' + lastDeal_orderMarket_price + ' продаем за ' + targetSellPrice)
  return targetSellPrice;
}

PriceCalculation.prototype.calculateSellPricesForStopOrder = async function(lastDeal_orderMarket_price) { /* done */
  let targetTriggerSellPrice = lastDeal_orderMarket_price * (100 + trade_parameters.brokerComissionPercents - trade_parameters.readyToLoosePercents)/(100 - trade_parameters.brokerComissionPercents)
  console.log('Купили за ' + lastDeal_orderMarket_price + ' продаем за ' + targetTriggerSellPrice + ' при падении цены. SL')
  return targetTriggerSellPrice ;
}


module.exports = {
  // movingAverageLong, 
  // movingAverageShort, 
  // calculateBuyPricesForLimitOrder, 
  // calculateBuyPricesForStopOrder, 
  // calculateSellPricesForLimitOrder, 
  // calculateSellPricesForStopOrder 
  PriceCalculation
}