/* здесь хранятся функции для создания заявок */
let request = require("request");

let {
  // track,
  // trade_parameters,
  // API_WARP_URL,
  API_WARPTRANS_URL,
  EXCHANGE,
  // DEFAULT_GET_HEADERS,
  DEFAULT_POST_HEADERS,
  DEFAULT_POST_OPTIONS,
  orderResponse,
  options
} = require('./parameters'); 

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

function OrdersPlace() {
}

OrdersPlace.prototype.postActionsMarketBuy = function postActionsMarketBuy(options_marketBuy) { /* done */

  return new Promise(function(resolve, reject) {
    request(options_marketBuy, function (error, response, body) {
      if (error) throw new Error(error);
      const market_response = body;//здесь body приходит как object
      market_response.httpCode = response.statusCode;
      resolve(market_response);
    });
  });
}

OrdersPlace.prototype.placeMarketOrderBuyAsync =  async function (volume,ticker,jwt) { /* done */
  
  // здесь, прямо внутри процедуры пишем токен в заголовок 

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/actions/market`; 
  
  const orderPlaceOptions = {
    ...DEFAULT_POST_OPTIONS,
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
      "X-ALOR-REQID": `${utilities.new_xalorreqid()}`
    },
    body:{
      ...(options.marketBuy.body),
      Quantity: `${volume}`,
      "Instrument": {
        "Symbol": `${ticker}`,
        "Exchange": EXCHANGE
      }
    },
    url
  };
  
  orderResponse.marketBuy = await this.postActionsMarketBuy(orderPlaceOptions);

  if (orderResponse.marketBuy.httpCode !== 200) {
    console.warn("Что-то не так с размещением заявки " + orderResponse.marketBuy);
  }
  
  if (orderResponse.marketBuy === orderResponse.stockExchangeIsClosed) {
    console.warn("Рынок закрыт, приходите завтра к 10:00");
  }
  return orderResponse.marketBuy;
}

OrdersPlace.prototype.postActionsLimitSell =  function (options_limitSell) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_limitSell, function (error, response, body) {
      if (error) throw new Error(error);
      const limit_response = (body);//здесь body приходит как object
      limit_response.httpCode = response.statusCode;
      resolve(limit_response);
    });
  });
}

OrdersPlace.prototype.placeLimitOrderSellAsync =  async function (targetSellPrice,volume,ticker,jwt) { /* done */
  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/actions/limit`; 
  
  const orderPlaceOptions = {
    ...DEFAULT_POST_OPTIONS,
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
      "X-ALOR-REQID": `${utilities.new_xalorreqid()}`
    },
    body:{
      ...options.limitSell.body,
      Quantity: `${volume}`,
      Price: `${targetSellPrice}`,
      "Instrument": {
        "Symbol": `${ticker}`,
        "Exchange": EXCHANGE
      }
    },
    url
  };

  orderResponse.limitSell = await this.postActionsLimitSell(orderPlaceOptions);
  if (orderResponse.limitSell.httpCode !== 200) {
    console.warn("Что-то не так с размещением заявки " + orderResponse.limitSell);
  }
  
  if (orderResponse.limitSell === orderResponse.stockExchangeIsClosed) {
    console.warn("Рынок закрыт, приходите завтра");
  }
  //await utilities.sleep(100);
  return orderResponse.limitSell;
}

OrdersPlace.prototype.postActionsStopLossSell =  function (options_stopLossSell) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_stopLossSell, function (error, response, body) {
      if (error) throw new Error(error);
      const stoploss_response = (body);//здесь body приходит как object
      stoploss_response.httpCode = response.statusCode;
      resolve(stoploss_response);
    });
  });
}

OrdersPlace.prototype.placeStopLossOrderSellAsync =  async function (targetTriggerSellPrice,volume,ticker,jwt) { /* done */
  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/actions/stoploss`; 
  
  const stopOrderPlaceOptions = {
    ...DEFAULT_POST_OPTIONS,
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
      "X-ALOR-REQID": `${utilities.new_xalorreqid()}`
    },
    body:{
      ...options.stopLossSell.body,
      Quantity: `${volume}`,
      TriggerPrice: `${targetTriggerSellPrice}`,
      "Instrument": {
        "Symbol": `${ticker}`,
        "Exchange": EXCHANGE
      }
    },
    url
  };

  orderResponse.stopLossSell = await this.postActionsStopLossSell(stopOrderPlaceOptions);
  
  if (orderResponse.stopLossSell.httpCode !== 200) {
    console.warn("Что-то не так с размещением заявки " + orderResponse.stopLossSell);
  }
  
  if (orderResponse.stopLossSell === orderResponse.stockExchangeIsClosed) {
    console.warn("Рынок закрыт, приходите завтра");
  }
  //await utilities.sleep(100);
  return orderResponse.stopLossSell;
}

OrdersPlace.prototype.postActionsMarketSell =  function (options_marketSell) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_marketSell, function (error, response, body) {
      if (error) throw new Error(error);
      const market_response = body;//здесь body приходит как object
      console.log(" =) ");
      console.log(response);
      market_response.httpCode = response.statusCode;
      resolve(market_response);
    });
  });
}

OrdersPlace.prototype.placeMarketOrderSellAsync =  async function (volume,ticker,jwt) { /* done */
  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/actions/market`; 
  
  const orderPlaceOptions = {
    ...DEFAULT_POST_OPTIONS,
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
      "X-ALOR-REQID": `${utilities.new_xalorreqid()}`
    },
    body:{
      ...(options.marketSell.body),
      Quantity: `${volume}`,
      "Instrument": {
        "Symbol": `${ticker}`,
        "Exchange": EXCHANGE
      }
    },
    url
  };
  
  orderResponse.marketSell = await this.postActionsMarketSell(orderPlaceOptions);
  
  if (orderResponse.marketSell === orderResponse.stockExchangeIsClosed) {
    console.warn("Рынок закрыт, приходите завтра");
  }
  
  if (orderResponse.marketSell.httpCode !== 200) {
    console.warn("Что-то не так с размещением заявки " + orderResponse.marketSell);
  }
  await utilities.sleep(100);
  return orderResponse.marketSell;
}

OrdersPlace.prototype.postActionsLimitBuy =  function (options_limitBuy) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_limitBuy, function (error, response, body) {
      if (error) throw new Error(error);
      const limit_response = (body);//здесь body приходит как object
      limit_response.httpCode = response.statusCode;
      resolve(limit_response);
    });
  });
}

OrdersPlace.prototype.placeLimitOrderBuyAsync =  async function (targetBuyPrice,volume,ticker,jwt) { /* done */
  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/actions/limit`; 
  
  const orderPlaceOptions = {
    ...DEFAULT_POST_OPTIONS,
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
      "X-ALOR-REQID": `${utilities.new_xalorreqid()}`
    },
    body:{
      ...options.limitBuy.body,
      Quantity: `${volume}`,
      Price: `${targetBuyPrice}`,
      "Instrument": {
        "Symbol": `${ticker}`,
        "Exchange": EXCHANGE
      },
    },
    url
  };

  orderResponse.limitBuy = await this.postActionsLimitBuy(orderPlaceOptions);
  if (orderResponse.limitBuy.httpCode !== 200) {
    console.warn("Что-то не так с размещением заявки " + orderResponse.limitBuy);
  }

  if (orderResponse.limitBuy === orderResponse.stockExchangeIsClosed) {
    console.warn("Рынок закрыт, приходите завтра");
  }
  //await utilities.sleep(100);
  return orderResponse.limitBuy;
}

OrdersPlace.prototype.postActionsStopLossBuy =  function (options_stopLossBuy) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_stopLossBuy, function (error, response, body) {
      if (error) throw new Error(error);
      const stoploss_response = (body);//здесь body приходит как object
      stoploss_response.httpCode = response.statusCode;
      resolve(stoploss_response);
    });
  });
}

OrdersPlace.prototype.placeStopLossOrderBuyAsync = async function (targetTriggerBuyPrice,volume,ticker,jwt) { /* done */
  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/actions/stoploss`; 
  
  const stopOrderPlaceOptions = {
    ...DEFAULT_POST_OPTIONS,
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
      "X-ALOR-REQID": `${utilities.new_xalorreqid()}`
    },
    body:{
      ...options.stopLossBuy.body,
      Quantity: `${volume}`,
      TriggerPrice: `${targetTriggerBuyPrice}`,
      "Instrument": {
        "Symbol": `${ticker}`,
        "Exchange": EXCHANGE
      }
    },
    url
  };

  orderResponse.stopLossBuy = await this.postActionsStopLossBuy(stopOrderPlaceOptions);

  if (orderResponse.stopLossBuy === orderResponse.stockExchangeIsClosed) {
    console.warn("Рынок закрыт, приходите завтра");
  }

  if (orderResponse.stopLossBuy.httpCode !== 200) {
    console.warn("Что-то не так с размещением заявки "+ orderResponse.stopLossBuy);
  }
  await utilities.sleep(100);
  return orderResponse.stopLossBuy;
}

module.exports = {
  // placeMarketOrderBuyAsync, 
  // placeLimitOrderSellAsync, 
  // placeStopLossOrderSellAsync, 
  // placeMarketOrderSellAsync, 
  // placeLimitOrderBuyAsync, 
  // placeStopLossOrderBuyAsync
  OrdersPlace
}