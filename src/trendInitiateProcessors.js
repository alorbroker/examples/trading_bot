let {
  //track,
  trade_parameters,
  orderResponse
} = require('./parameters'); 

let {
  // calculateBuyPricesForLimitOrder, 
  // calculateBuyPricesForStopOrder, 
  // calculateSellPricesForLimitOrder, 
  // calculateSellPricesForStopOrder 
  PriceCalculation
} = require('./priceCalculation'); 

const priceCalculation = new PriceCalculation();

let {
  // placeMarketOrderBuyAsync, 
  // placeLimitOrderSellAsync, 
  // placeStopLossOrderSellAsync, 
  // placeMarketOrderSellAsync, 
  // placeLimitOrderBuyAsync, 
  // placeStopLossOrderBuyAsync
  OrdersPlace
} = require('./ordersPlace'); 

const ordersPlace = new OrdersPlace();

let {
  OrdersRemoval
} = require('./OrdersRemoval'); 

const ordersRemoval = new OrdersRemoval();

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

let {
  // ordersStatus.obtainStatusMarketOrderBuyAsync, 
  // ordersStatus.obtainStatusLimitOrderSellAsync, 
  // ordersStatus.obtainStatusStopLossOrderSellAsync, 
  // ordersStatus.obtainStatusMarketOrderSellAsync, 
  // ordersStatus.obtainStatusLimitOrderBuyAsync, 
  // ordersStatus.obtainStatusStopLossOrderBuyAsync
  OrdersStatus
} = require('./ordersStatus'); 

const ordersStatus = new OrdersStatus();

function TrendInitiateProcessors() {}

TrendInitiateProcessors.prototype.processCaseBearTrendInitiate = async function (track){
  let targetBuyPrice;
  let targetTriggerBuyPrice;
  
  // console.log('Индикатор MACD сигнализирует: красный превысил синего на дельту, цена падает, продаём');
  console.log('Индикатор MACD сигнализирует: цена падает, продаём');
  orderResponse.marketSell = await ordersPlace.placeMarketOrderSellAsync(track.trade_parameters.volume,track.trade_parameters.ticker,track.jwt); // разместили, но могло быть 400 или rejected
  await utilities.sleep(trade_parameters.smalldelay);
  
  if (orderResponse.marketSell.httpCode !== 200) {
    console.warn("Робот пытался отправить рыночную заявку, но вот что ему ответили");
    console.log(orderResponse.marketSell);
    track.areWeWaitingForTakeProfit = false;
    track.isCheckIfItIsTimeForTradeNeeded = true;
    //ОШИБКА: (133) Торги по этому финансовому инструменту сейчас не проводятся.
    return track;
  }
  track.lastDeal.orderMarket = await ordersStatus.obtainStatusMarketOrderSellAsync(orderResponse.marketSell.orderNumber,track.jwt); // исполнена, отклонена, что-то ещё?
  console.log("Рыночная заявка статус: " + track.lastDeal.orderMarket.status);
  
  if (track.lastDeal.orderMarket.status === 'rejected') {
    console.warn("Рыночная заявка отвергнута. Вы уверены что сейчас ведутся торги?");
    track.areWeWaitingForTakeProfit = false;
    return track;
  }
  for (let waitMarketFill = 0; waitMarketFill < trade_parameters.waitMarketFillMax; waitMarketFill++) {
    
    if (track.lastDeal.orderMarket.status === 'filled') {//а вдруг надо ждать пока будет filled если рынок неликвиден? 
      targetBuyPrice = await priceCalculation.calculateBuyPricesForLimitOrder(track.lastDeal.orderMarket.price);
      orderResponse.limitBuy = await ordersPlace.placeLimitOrderBuyAsync(targetBuyPrice,track.trade_parameters.volume,track.trade_parameters.ticker,track.jwt);
      track.lastDeal.orderLimit = await ordersStatus.obtainStatusLimitOrderBuyAsync(orderResponse.limitBuy.orderNumber,track.jwt); // исполнена, отклонена, что-то ещё?
      targetTriggerBuyPrice = await priceCalculation.calculateBuyPricesForStopOrder(track.lastDeal.orderMarket.price);
      orderResponse.stopLossBuy = await ordersPlace.placeStopLossOrderBuyAsync(targetTriggerBuyPrice,track.trade_parameters.volume,track.trade_parameters.ticker,track.jwt);
      track.lastDeal.orderStopLoss = await ordersStatus.obtainStatusStopLossOrderBuyAsync(orderResponse.stopLossBuy.orderNumber,track.jwt); // исполнена, отклонена, что-то ещё?
      
      if (track.lastDeal.orderLimit.status === 'rejected') {
        console.log('Лимитная заявка на покупку отвергнута, теперь робот не купит проданую акцию'); await utilities.sleep(trade_parameters.smalldelay);
        track.areWeWaitingForTakeProfit = false;
        return track;
      } else if (track.lastDeal.orderStopLoss.status === 'rejected') {
        console.log('Стоплосс заявка отвергнута, теперь робот не купит проданую акцию '); await utilities.sleep(trade_parameters.smalldelay);
        track.areWeWaitingForTakeProfit = false;
        return track;
      } else if ((track.lastDeal.orderStopLoss.status === 'working' || track.lastDeal.orderStopLoss.status === 'filled')&&(track.lastDeal.orderLimit.status === 'working' || track.lastDeal.orderLimit.status === 'filled')) {
        console.log('Заявки на покупку(limit и stoploss) выставлены, перехожу в режим ожидания исполнения заявок'); await utilities.sleep(trade_parameters.smalldelay);
        track.areWeWaitingForTakeProfit = true;
        return track;
      } else {//(track.lastDeal.orderLimit.status === undefined || track.lastDeal.orderStopLoss.status === undefined)
        console.log('Похоже сервер не ответил на запрос на выставление лимит и стоплосс заявок. Рыночная заявка на продажу уже выполнилась ')
        track.areWeWaitingForTakeProfit = false;
        return track;
      } 
    } 
    await utilities.sleep(trade_parameters.smalldelay);
    await utilities.sleep(trade_parameters.smalldelay);
    track.lastDeal.orderMarket = await ordersStatus.obtainStatusMarketOrderSellAsync(orderResponse.marketSell.orderNumber,track.jwt); // исполнена, отклонена, что-то ещё?
    console.log("Рыночная заявка статус: "+ track.lastDeal.orderMarket.status);
  }
  
  if (track.lastDeal.orderMarket.status !== 'filled') {
    console.log("Робот сделал рыночную заявку, и ожидал некоторое время, но она не реализовалась. Вы уверены что инструменты достаточно ликвидны?");
    orderResponse.delete = await ordersRemoval.removeMarketOrderSellAsync(orderResponse.marketBuy.orderNumber,track.jwt);
    
    if (orderResponse.delete.httpCode === 200) {
      console.log("Робот отозвал эту рыночную заявку. ");
    } else {
      console.log("Робот попытался отозвать эту рыночную заявку, но неудачно");
    }
    track.areWeWaitingForTakeProfit = false;
    return track;
  }
}

TrendInitiateProcessors.prototype.processCaseBullTrendInitiate = async function (track){
  let targetSellPrice;
  let targetTriggerSellPrice;
  
  // console.log('Индикатор MACD сигнализирует: синий превысил красного на дельту, цена растет, покупаем');
  console.log('Индикатор MACD сигнализирует: цена растет, покупаем');
  orderResponse.marketBuy = await ordersPlace.placeMarketOrderBuyAsync(track.trade_parameters.volume,track.trade_parameters.ticker,track.jwt); 
  await utilities.sleep(trade_parameters.smalldelay);
  
  if (orderResponse.marketBuy.httpCode !== 200) {
    console.warn("Робот пытался отправить рыночную заявку, но вот что ему ответили");
    console.log(orderResponse.marketBuy);
    track.areWeWaitingForTakeProfit = false;
    track.isCheckIfItIsTimeForTradeNeeded = true;
    return track;
  }
  track.lastDeal.orderMarket = await ordersStatus.obtainStatusMarketOrderBuyAsync(orderResponse.marketBuy.orderNumber,track.jwt); 
  console.log("Рыночная заявка статус: " + track.lastDeal.orderMarket.status);
  
  if (track.lastDeal.orderMarket.status === 'rejected') {
    console.warn("Рыночная заявка отвергнута. Вы уверены что сейчас ведутся торги?");
    track.areWeWaitingForTakeProfit = false;
    return track;
  }
  for ( let waitMarketFill = 0; waitMarketFill < trade_parameters.waitMarketFillMax; waitMarketFill++) {
    
    if (track.lastDeal.orderMarket.status === 'filled') {
      targetSellPrice = await priceCalculation.calculateSellPricesForLimitOrder(track.lastDeal.orderMarket.price);
      orderResponse.limitSell = await ordersPlace.placeLimitOrderSellAsync(targetSellPrice,track.trade_parameters.volume,track.trade_parameters.ticker,track.jwt);
      track.lastDeal.orderLimit = await ordersStatus.obtainStatusLimitOrderSellAsync(orderResponse.limitSell.orderNumber,track.jwt); 
      targetTriggerSellPrice = await priceCalculation.calculateSellPricesForStopOrder(track.lastDeal.orderMarket.price);
      orderResponse.stopLossSell = await ordersPlace.placeStopLossOrderSellAsync(targetTriggerSellPrice,track.trade_parameters.volume,track.trade_parameters.ticker,track.jwt);
      track.lastDeal.orderStopLoss = await ordersStatus.obtainStatusStopLossOrderSellAsync(orderResponse.stopLossSell.orderNumber,track.jwt); 
      
      if (track.lastDeal.orderLimit.status === 'rejected') {
        console.log('Лимитная заявка на покупку отвергнута, теперь робот не продаст купленую акцию'); 
        await utilities.sleep(trade_parameters.smalldelay);
        track.areWeWaitingForTakeProfit = false;
        return track;
      } else if (track.lastDeal.orderStopLoss.status === 'rejected') {
        console.log('Стоплосс заявка отвергнута, теперь робот не продаст купленую акцию '); 
        await utilities.sleep(trade_parameters.smalldelay);
        track.areWeWaitingForTakeProfit = false;
        return track;   
      } else if (
        (track.lastDeal.orderStopLoss.status === 'working' || track.lastDeal.orderStopLoss.status === 'filled')
        &&(track.lastDeal.orderLimit.status === 'working' || track.lastDeal.orderLimit.status === 'filled')) {
        console.log('Заявки на продажу(limit и stoploss) выставлены, перехожу в режим ожидания исполнения заявок'); 
        await utilities.sleep(trade_parameters.smalldelay);
        track.areWeWaitingForTakeProfit = true;
        return track;
      } else { 
        console.log('Похоже сервер не ответил на запрос на выставление лимит и стоплосс заявок. Рыночная заявка на покупку уже выполнилась ')
        track.areWeWaitingForTakeProfit = false;
        return track;
      } 
    } 
    await utilities.sleep(trade_parameters.smalldelay);//
    await utilities.sleep(trade_parameters.smalldelay);
    track.lastDeal.orderMarket = await ordersStatus.obtainStatusMarketOrderBuyAsync(orderResponse.marketBuy.orderNumber,track.jwt); 
    console.log("Рыночная заявка размещена и имеет статус: "+ track.lastDeal.orderMarket.status);
  }
  
  if (track.lastDeal.orderMarket.status !== 'filled') {
    console.log("Робот сделал рыночную заявку, и ожидал некоторое время, но она не реализовалась. Вы уверены что инструменты достаточно ликвидны?");
    orderResponse.delete = await ordersRemoval.removeMarketOrderBuyAsync(orderResponse.marketBuy.orderNumber,track.jwt);
    
    if (orderResponse.delete.httpCode === 200) {
      console.log("Робот отозвал эту рыночную заявку. ");
    } else {
      console.log("Робот попытался отозвать эту рыночную заявку, но неудачно");
    }
    track.areWeWaitingForTakeProfit = false;
    return track;
  }
}

module.exports = {
  // processCaseBearTrendInitiate,
  // processCaseBullTrendInitiate
  TrendInitiateProcessors
}
