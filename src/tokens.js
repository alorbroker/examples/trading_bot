
let request = require("request");

let {
  DEFAULT_POST_HEADERS,
  DEFAULT_POST_OPTIONS
} = require('./parameters'); 

function Tokens() {
};

Tokens.prototype.postToObtainJWTToken = function (options_gettoken) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_gettoken, function (error, response, body) {
      if (error) throw new Error(error);
      if (response.statusCode !== 200) {
        console.error("Что-то не так с обновлением jwt токена, ");
      }
      let tokenPackage = body;
      resolve(tokenPackage);
    });
  });
}

Tokens.prototype.setTokenAsync = async function(track) { /* done */

  const jwtTokenOptions = {
    ...DEFAULT_POST_OPTIONS,
    
    url: `https://oauthdev.alor.ru/refresh?token=${track.refreshToken}`, 
    
    headers: {
      "Content-Type": DEFAULT_POST_HEADERS["Content-Type"]
    }
  };

  //console.log("Получаем новый токен...");
  
  let tokenPackage = await this.postToObtainJWTToken(jwtTokenOptions);
  //console.log("Удалось получить jwt токен : " + tokenPackage.AccessToken );

  if(tokenPackage.errorMessage === null || tokenPackage.errorMessage === undefined){
    track.jwt = tokenPackage.AccessToken;
    console.log("Токен успешно обновлён " + track.jwt.toString().slice(-8));
    // console.log(track.jwt.toString());
  } else {
    console.error("Не удалось получить токен " + tokenPackage.errorMessage);
    track.jwt = null;
  }
  return ;
}

module.exports = {
  Tokens
}