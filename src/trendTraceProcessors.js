let {
  trade_parameters,
  orderResponse
} = require('./parameters'); 

let {
  // ordersRemoval.removeLimitOrderBuyAsync, 
  // ordersRemoval.removeLimitOrderSellAsync, 
  // ordersRemoval.removeStopLossOrderBuyAsync, 
  // ordersRemoval.removeStopLossOrderSellAsync  
  OrdersRemoval
} = require('./ordersRemoval'); 

const ordersRemoval = new OrdersRemoval();

let {
  // ordersStatus.obtainStatusLimitOrderSellAsync, 
  // ordersStatus.obtainStatusStopLossOrderSellAsync, 
  // ordersStatus.obtainStatusLimitOrderBuyAsync, 
  // ordersStatus.obtainStatusStopLossOrderBuyAsync
  OrdersStatus
} = require('./ordersStatus'); 

const ordersStatus = new OrdersStatus();

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

function TrendTraceProcessors() {
}

TrendTraceProcessors.prototype.processCaseFilledLimitOrderBuy = async function (track){
  orderResponse.delete = await ordersRemoval.removeStopLossOrderBuyAsync(track.lastDeal.orderStopLoss.id,track.jwt);
    
  if (orderResponse.delete.httpCode === 200) {
    console.log('Исполнилась лимитная заявка на покупку. Прибыль зафиксирована. Стоплосс заявка отменена');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
  } else if (orderResponse.delete.message === orderResponse.alreadyActivated) {
    console.log('Исполнилась лимитная заявка на покупку. Прибыль зафиксирована. Стоплосс заявку не удалось отменить, тк она успела активироваться ');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  } else {
    console.log('Исполнилась лимитная заявка на покупку. Прибыль зафиксирована. Стоплосс заявку не удалось отменить, по неизвестным причинам');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  }
  await utilities.sleep(trade_parameters.smalldelay);
  console.log('Перехожу в режим ожидания сигнала от индикатора');
  track.areWeWaitingForTakeProfit = false; //заявка исполнена, теперь ждём сигнала индикатора
  return track;       
}

TrendTraceProcessors.prototype.processCaseFilledStopLossOrderBuy = async function (track){
  orderResponse.delete = await ordersRemoval.removeLimitOrderBuyAsync(track.lastDeal.orderLimit.id,track.jwt);
    
  if (orderResponse.delete.httpCode === 200 || orderResponse.delete.message === orderResponse.alreadyDeleted) {
    console.log('Исполнилась стоплосс заявка. Убытки зафиксированы. Лимитная заявка отменена');  
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  } else {
    console.log('Исполнилась стоплосс заявка. Убытки зафиксированы. Лимитную заявку не удалось отменить');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  }
  await utilities.sleep(trade_parameters.smalldelay);
  track.areWeWaitingForTakeProfit = false;
  return track;
}

TrendTraceProcessors.prototype.processCaseCanceledOrder = async function (track){
  
  if (track.lastDeal.orderLimit.status === 'canceled') {
    console.warn('Кто-то отменил лимитную заявку (не этот робот), вероятно система.');
    await utilities.sleep(trade_parameters.smalldelay);
  }
  
  if (track.lastDeal.orderStopLoss.status === 'canceled') {
    console.warn('Кто-то отменил стоплосс заявку (не этот робот), вероятно система.');
    await utilities.sleep(trade_parameters.smalldelay);
  }
  track.areWeWaitingForTakeProfit = false;
  return track;
}

TrendTraceProcessors.prototype.processCaseBothFilledOrder = async function (track){
  console.log('Исполнились обе заявки, и лимитная и стоплосс. ');
  track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
  track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  await utilities.sleep(trade_parameters.smalldelay);
  console.log('Перехожу в режим ожидания сигнала от индикатора');
  await utilities.sleep(trade_parameters.smalldelay);
  track.areWeWaitingForTakeProfit = false;
  return track;
}

TrendTraceProcessors.prototype.processCaseBearTrendTrace = async function (track){
  
  if (track.lastDeal.orderLimit.status === 'foobar' && track.lastDeal.orderStopLoss.status === 'foobar' && track.lastDeal.orderMarket.status === 'foobar') {
    console.log('Начался новый торговый день ')
    track.areWeWaitingForTakeProfit = false;
    return;
  } else {  
    track.lastDeal.orderStopLoss = await ordersStatus.obtainStatusStopLossOrderBuyAsync(track.lastDeal.orderStopLoss.id,track.jwt);
    track.lastDeal.orderLimit = await ordersStatus.obtainStatusLimitOrderBuyAsync(track.lastDeal.orderLimit.id,track.jwt);
  }  
  
  if (track.lastDeal.orderLimit.status === 'filled' && track.lastDeal.orderStopLoss.status !== 'filled') { 
    track = await this.processCaseFilledLimitOrderBuy(track);     
  } else if (track.lastDeal.orderLimit.status !== 'filled' && track.lastDeal.orderStopLoss.status === 'filled') {
    track = await this.processCaseFilledStopLossOrderBuy(track);
  } else if (track.lastDeal.orderLimit.status === 'working' && track.lastDeal.orderStopLoss.status === 'working') {
    //console.log('Заявки стоплосс и лимитная ожидают исполнения, держу Вас в курсе');
    await utilities.sleep(trade_parameters.smalldelay);
    track.areWeWaitingForTakeProfit = true;
  } else if (track.lastDeal.orderLimit.status === 'canceled' || track.lastDeal.orderStopLoss.status === 'canceled') {
    track = await this.processCaseCanceledOrder(track);
  } else if (track.lastDeal.orderLimit.status === 'filled' && track.lastDeal.orderStopLoss.status === 'filled') {
    track = await this.processCaseBothFilledOrder(track);
  } else {
    console.warn('Неклассифицированная ситуация. Её происходить не должно, тем не менее. Статусы лимитной и стоплосс заявок: '+track.lastDeal.orderLimit.status+track.lastDeal.orderStopLoss.status);
    await utilities.sleep(trade_parameters.smalldelay);
  }
  return track;
}

TrendTraceProcessors.prototype.processCaseFilledLimitOrderSell = async function (track){
  orderResponse.delete = await ordersRemoval.removeStopLossOrderSellAsync(track.lastDeal.orderStopLoss.id,track.jwt); 
  
  if (orderResponse.delete.httpCode === 200) {
    console.log('Исполнилась лимитная заявка на продажу. Прибыль зафиксирована. Стоплосс заявка отменена');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
  } else if (orderResponse.delete.message === orderResponse.alreadyActivated) {
    console.log('Исполнилась лимитная заявка на продажу. Прибыль зафиксирована. Стоплосс заявку не удалось отменить, тк она успела активироваться ');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  } else {
    console.log('Исполнилась лимитная заявка на продажу. Прибыль зафиксирована. Стоплосс заявку не удалось отменить, по неизвестным причинам');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  }
  await utilities.sleep(trade_parameters.smalldelay);
  track.areWeWaitingForTakeProfit = false; //заявка исполнена, теперь ждём сигнала индикатора
  return track;
}

TrendTraceProcessors.prototype.processCaseFilledStopLossOrderSell = async function (track){
  orderResponse.delete = await ordersRemoval.removeLimitOrderSellAsync(track.lastDeal.orderLimit.id,track.jwt);

  if (orderResponse.delete.httpCode === 200 || orderResponse.delete.message === orderResponse.alreadyDeleted) {
    console.log('Исполнилась стоплосс заявка. Убытки зафиксированы. Лимитная заявка отменена');  
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  } else {
    console.log('Исполнилась стоплосс заявка. Убытки зафиксированы. Лимитную заявку не удалось отменить');
    track.relativeGainPercents = track.relativeGainPercents + trade_parameters.toWinPercents;
    track.relativeGainPercents = track.relativeGainPercents - trade_parameters.readyToLoosePercents;
  }
  await utilities.sleep(trade_parameters.smalldelay);
  track.areWeWaitingForTakeProfit = false;
  return track;
}

TrendTraceProcessors.prototype.processCaseBullTrendTrace = async function (track) {
  if (track.lastDeal.orderLimit.status === 'foobar' && track.lastDeal.orderStopLoss.status === 'foobar' && track.lastDeal.orderMarket.status === 'foobar') {
    console.log('Начался новый торговый день ')
    track.areWeWaitingForTakeProfit = false;
    return;
  } else {  
    track.lastDeal.orderStopLoss = await ordersStatus.obtainStatusStopLossOrderSellAsync(track.lastDeal.orderStopLoss.id,track.jwt);
    track.lastDeal.orderLimit = await ordersStatus.obtainStatusLimitOrderSellAsync(track.lastDeal.orderLimit.id,track.jwt);
  }
  
  if (track.lastDeal.orderLimit.status === 'filled' && (track.lastDeal.orderStopLoss.status !== 'filled')) { 
    track = await this.processCaseFilledLimitOrderSell(track);
  } else if (track.lastDeal.orderLimit.status !== 'filled' && track.lastDeal.orderStopLoss.status === 'filled') {
    track = await this.processCaseFilledStopLossOrderSell(track);
  } else if (track.lastDeal.orderLimit.status === 'working' && track.lastDeal.orderStopLoss.status === 'working') {
    //console.log('Заявки стоплосс и лимитная ожидают исполнения, держу Вас в курсе')
    track.areWeWaitingForTakeProfit = true;
  } else if (track.lastDeal.orderLimit.status === 'canceled' || track.lastDeal.orderStopLoss.status === 'canceled') {
    track = await this.processCaseCanceledOrder(track);
  } else if (track.lastDeal.orderLimit.status === 'filled' && track.lastDeal.orderStopLoss.status === 'filled') {
    track = await this.processCaseBothFilledOrder(track);
  } else {
    console.warn('Неклассифицированная ситуация. Её происходить не должно, тем не менее. Статусы лимитной и стоплосс заявок : '+track.lastDeal.orderLimit.status+track.lastDeal.orderStopLoss.status);
    await utilities.sleep(trade_parameters.smalldelay);
  }
  return ;
}

module.exports = {
  // processCaseBearTrendTrace,
  // processCaseBullTrendTrace
  TrendTraceProcessors
}