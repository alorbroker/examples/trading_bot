/* здесь хранятся крупные процедуры */

const request = require("request");

let {
  trade_parameters
} = require('./parameters'); 

let {
  // trendTraceProcessors.processCaseBearTrendTrace,
  // trendTraceProcessors.processCaseBullTrendTrace
  TrendTraceProcessors
} = require('./trendTraceProcessors');

const trendTraceProcessors = new TrendTraceProcessors();

let {
  // trendInitiateProcessors.processCaseBearTrendInitiate,
  // trendInitiateProcessors.processCaseBullTrendInitiate
  TrendInitiateProcessors
} = require('./trendInitiateProcessors');

trendInitiateProcessors = new TrendInitiateProcessors();

const History = require('./priceHistory');

const history = new History(request);

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

let {
  Tokens
} = require('./tokens');

const tokens = new Tokens();

let {
  subscribeToWebSocket
} = require('./webSocket');

let {
  // movingAverageLong, 
  // movingAverageShort, 
  PriceCalculation
} = require('./priceCalculation');

const priceCalculation = new PriceCalculation();

function Procedures(){}

Procedures.prototype.checkIfItIsTimeForTrade = async function(track){
  track.isItTimeForTrade = true;
  currentServerTime = await utilities.callTime(track.jwt);

  if((new Date(1000 * (currentServerTime))).toString().substring(0,3) === 'Sat'){
    track.isItTimeForTrade = false; /* В субботу не работаем */
    console.log('В субботу     биржа не работает. Робот будет приостановлен на 24 часа. ')
    await utilities.sleep(1000 * (24 * 3600));
    await tokens.setTokenAsync(track);
    subscribeToWebSocket(track); 
  }

  if((new Date(1000 * (currentServerTime))).toString().substring(0,3) === 'Sun'){
    track.isItTimeForTrade = false; /* В воскресенье не работаем */
    console.log('В воскресенье биржа не работает. Робот будет приостановлен на 8 часов. ')
    await utilities.sleep(1000 * (8 * 3600));
    /* по идее ещё надо обработать случаи праздников, но это сложно*/
    await tokens.setTokenAsync(track); /* подписка не задублируется так как в субботу и воскресенье вебсокет соединение закрывается сразу*/
    subscribeToWebSocket(track); 
  }
  currentServerTime = await utilities.callTime(track.jwt);

  if ((parseInt((new Date(1000 * currentServerTime)).toString().substring(16,18),10)) * 60 + parseInt((new Date(1000 * currentServerTime)).toString().substring(19,21),10)
    < parseInt(trade_parameters.hhmmStart.substring(0,2),10) * 60 + parseInt(trade_parameters.hhmmStart.substring(3,5),10)) {
      track.isItTimeForTrade = false; /*здесь вычислено кол-во минут с полуночи и проверено что мы НЕ находимся в заданном интервале торгов */
  }

  if ((parseInt((new Date(1000 * currentServerTime)).toString().substring(16,18),10)) * 60 + parseInt((new Date(1000 * currentServerTime)).toString().substring(19,21),10)
    > parseInt(trade_parameters.hhmmEnd.substring(0,2),10) * 60 + parseInt(trade_parameters.hhmmEnd.substring(3,5),10)) {
      track.isItTimeForTrade = false; /*здесь вычислено кол-во минут с полуночи и проверено что мы НЕ находимся в заданном интервале торгов */
  }
  
  if (track.isItTimeForTrade !== true) {
    console.log('Сейчас ' + (new Date(1000 * currentServerTime)).toString().substring(16,24) + " мы вне временного интервала, выбраного для торговли: с "+trade_parameters.hhmmStart+" до "+trade_parameters.hhmmEnd);
  
    if ((parseInt((new Date(1000 * currentServerTime)).toString().substring(16,18),10)) * 60 + parseInt((new Date(1000 * currentServerTime)).toString().substring(19,21),10)
      > parseInt(('19:30').substring(0,2),10) * 60 + parseInt(('19:30').substring(3,5),10)) {
      console.log('Похоже, что рынок закрыт, робот будет приостановлен на срок 13 часов, примерно до 08:30');
      await utilities.sleep(1000 * (13 * 3600 ));
      await tokens.setTokenAsync(track);
      subscribeToWebSocket(track); 
    }
  } else {
    // console.log('Сейчас ' + (new Date(1000 * currentServerTime)).toString().substring(16,24) + " мы во  временном  интервале, выбраном  для торговли: с "+trade_parameters.hhmmStart+" до "+trade_parameters.hhmmEnd);
    console.log(`Сейчас ${(new Date(1000 * currentServerTime)).toString().substring(16,24)} мы во  временном  интервале, выбраном  для торговли: с ${trade_parameters.hhmmStart} до ${trade_parameters.hhmmEnd}`);
  }
  return ;
}

Procedures.prototype.elementaryTradeStep = async function(track) {
  
  track.localHistory = await history.obtainHistoryAsync(track);
  
  if (track.wsHistory.length === 0) {
    //console.log( `сессия закрыта.`);
    track.isCheckIfItIsTimeForTradeNeeded = true;
    return;
  }
  
  if (track.previousLastHistoryTimeframeValue === track.localHistory.history[(track.localHistory.history).length-1].time) {
    console.log('Новая свеча ещё не сформировалась, но можно проверить заявки');
    await utilities.sleep(trade_parameters.smalldelay);
    
    if (track.areWeWaitingForTakeProfit === true) {
      // track = await this.checkOrdersStatusesAsync(track);
      await this.checkOrdersStatusesAsync(track);
    } else if (track.areWeWaitingForTakeProfit !== true) {
      console.log('Заявок не выставлено, проверять нечего.');
      await utilities.sleep(trade_parameters.smalldelay);
    }
  } else if (  (track.localHistory.history).length === 0) {
    console.log('Не удалось получить историю');
    await utilities.sleep(trade_parameters.smalldelay);
  } else {
    console.log('Новая свеча сформировалась, обрабатываю');
    await utilities.sleep(trade_parameters.smalldelay);
    
    // console.log('What is in History #last ',track.localHistory.history[(track.localHistory.history).length-1]);
    // track.localHistory.history[(track.localHistory.history).length-1]
    
    if ( isNaN(track.currentLongMAValue) || isNaN(track.currentShortMAValue) ) {
      // костыль
      // NaN это инициальное значение current LongMAValue и current ShortMAValue
      // значит вначале, мы встретимся с ситуацией, когда там NaN
      // и если записать туда скользящее среднее из текущего актуального значения
      // и если записать туда скользящее среднее из сдвинутой истории, то всё будет правильно
      // но этот костыль также отработает, если окажется что в индикаторе появились NaN по иным причинам
      console.log('MACD met NaN, trying to recalculate.'); //если это произойдёт только один раз при запуске, то ок.
      track.previousLongMAValue = priceCalculation.movingAverageLong( track.localHistory.history.slice(0 , (track.localHistory.history).length-1) );
      track.previousShortMAValue = priceCalculation.movingAverageShort( track.localHistory.history.slice(0 , (track.localHistory.history).length-1) ); 
      
      track.currentLongMAValue = priceCalculation.movingAverageLong(track.localHistory.history);
      track.currentShortMAValue = priceCalculation.movingAverageShort(track.localHistory.history); 
    
      // end костыль
    } else {
      track.previousLongMAValue = track.currentLongMAValue;   // 
      track.previousShortMAValue = track.currentShortMAValue; //

      track.currentLongMAValue = priceCalculation.movingAverageLong(track.localHistory.history);
      track.currentShortMAValue = priceCalculation.movingAverageShort(track.localHistory.history); 
    }
    if (track.areWeWaitingForTakeProfit === true) {
      // track = await this.checkOrdersStatusesAsync(track);
      await this.checkOrdersStatusesAsync(track);
    } else if (track.areWeWaitingForTakeProfit === false) { 
      // track = await this.callMacdIndicatorAsync(track);
      await this.callMacdIndicatorAsync(track);
    } else {
      console.log(`areWeWaitingForTakeProfit не равен true или false, а равен ${track.areWeWaitingForTakeProfit}`)
    }
  }
}

Procedures.prototype.checkOrdersStatusesAsync = async function(track) {
  console.log('Режим ожидания исполнения выставленных заявок');
  await utilities.sleep(trade_parameters.smalldelay);
  //console.log('Проверка исполнения выставленных заявок');
  await utilities.sleep(trade_parameters.smalldelay);
  
  if (track.lastDeal.orderMarket.side === 'buy') { 
    // track = await trendTraceProcessors.processCaseBullTrendTrace(track);
    await trendTraceProcessors.processCaseBullTrendTrace(track);
  } else { 
    // track = await trendTraceProcessors.processCaseBearTrendTrace(track);
    await trendTraceProcessors.processCaseBearTrendTrace(track);
  } 
  return ;
  // return track;
}

Procedures.prototype.callMacdIndicatorAsync = async function(track) {
  console.log('Режим ожидания сигнала от индикатора. Заявок не выставлено, проверяем сигнал индикатора MACD');
  await utilities.sleep(trade_parameters.smalldelay);
  
  if ( isNaN(track.previousLastHistoryTimeframeValue) === false && track.currentLongMAValue + trade_parameters.deltaMacdRub < track.currentShortMAValue) {
  
    await trendInitiateProcessors.processCaseBullTrendInitiate(track)
    return ;    
  
  } else if (isNaN(track.previousLastHistoryTimeframeValue) === false && track.currentLongMAValue > track.currentShortMAValue + trade_parameters.deltaMacdRub) {
  
    await trendInitiateProcessors.processCaseBearTrendInitiate(track);
    return ;
  
  } else if (track.previousLongMAValue <= track.previousShortMAValue + trade_parameters.deltaMacdRub && track.currentLongMAValue <= track.currentShortMAValue + trade_parameters.deltaMacdRub) { 
  
    console.log(`MACD индикатор не подаёт сигнал: различие MA не превысило порог в ${trade_parameters.deltaMacdRub} руб.`);
    await utilities.sleep(trade_parameters.smalldelay);
  
  } else if (track.previousLongMAValue + trade_parameters.deltaMacdRub >= track.previousShortMAValue && track.currentLongMAValue + trade_parameters.deltaMacdRub>= track.currentShortMAValue) {
  
    console.log(`MACD индикатор не подаёт сигнал: различие MA не превысило порог в ${trade_parameters.deltaMacdRub} руб.`);
    await utilities.sleep(trade_parameters.smalldelay);
  
  } else {
    
    if ( isNaN(track.previousLongMAValue) === true && isNaN(track.previousShortMAValue) === true && isNaN(track.currentLongMAValue) === false && isNaN(track.currentShortMAValue) === false ) {   
      console.error(`Значения NaN`);
    } else {
      console.log(`Неклассифицированная ситуация. Её происходить не должно. Индикаторы (длинный короткий): ${track.currentLongMAValue} ${track.currentShortMAValue}`);
      await utilities.sleep(trade_parameters.smalldelay);
      console.log(' перечисление значений истории : ' + track.localHistory.history[0].close + ' ' + track.localHistory.history[(localHistory.history).length-1].close);
      await utilities.sleep(trade_parameters.smalldelay);
    }
  }
  track.areWeWaitingForTakeProfit = false;
  return ;
  // return track;
}

module.exports = {
  // // checkTroublesParameters, 
  // checkIfItIsTimeForTrade,
  // elementaryTradeStep
  Procedures
}
