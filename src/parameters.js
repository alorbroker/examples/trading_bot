/* здесь хранятся параметры, а также описания объектов, которые используются для передачи данных между функциями*/

const API_WARP_URL = 'https://apidev.alor.ru/md';
const API_WARPTRANS_URL = 'https://apidev.alor.ru/warptrans';
const EXCHANGE = 'GAME';
const DEFAULT_GET_HEADERS = {
  
};
const DEFAULT_POST_HEADERS = {
  'Content-Type': 'application/json'  
};
const DEFAULT_POST_OPTIONS = {
  method: 'POST',  
  json: true
};

const trade_parameters = {
  brokerComissionPercents: 0.05, // комиссия, отчисляемая в пользу брокера, проценты
  readyToLoosePercents: 0.35, // степень риска - на столько процентов уменьшится баланс после закрытия позиции () 
  toWinPercents: 0.01, // на столько процентов увеличится баланс после закрытия позиции, рекомендуется 0.07
  //qty: 1, // объём открываемой позиции
  longMALength: 15, // параметры осциллятора MACD c экспоненциальным подавлением
  shortMALength: 7, // длинный и короткий участки истории соответственно
  exponentialDamper: 0.02, //параметр b. EMA среднее берется с весом exp (-b*t). оптимально в диапазоне от 0.01 до 0.5
  hhmmStart: "10:42", // не торговать до(в пределах текущих суток) этого времени
  hhmmEnd: "18:28", // не торговать после(в пределах текущих суток) этого времени
  deltaMacdRub: 0.002, // чем больше это значение, тем меньше шанс получить ложный сигнал от MACD (и тем больше задержка сигнала MACD)
  smalldelay: 100, // мс задержка после вывода сообщения в лог
  waitForWebSocketTries: 20,
  waitMarketFillMax: 3, // количество повторов запросов в ожидании исполнения (переход из статуса working в статус filled) рыночной заявки
  mainLoopDelay: 1025//9116//4173 // задержка в главном цикле, миллисекунды
}

let track = {
  jwt: "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJQMDM5MDA0IiwiZW50IjoiY2xpZW50IiwiZWluIjoiMzgyMTIiLCJjbGllbnRJZCI6IjExNTU3NyIsImF6cCI6IjFkNDA0YmZjNzA1NTRlNjNhNmMxIiwiYWdyZWVtZW50cyI6IjM5MDA0IDQ5MzAwIDYwMTMwIDYwNTY4IDYwNTY5IDYwNTcwIDYwNTcxIDYwNTg2IDYwNTg3IDYwNTg5IDYwNjU4IDYwNjU5IDYwNjYwIDYwNjYxIDYwNjYyIDYwNjYzIDYwNjY0IDYwNjY1IDYwNjY2IDYwNjY3IDYwNjY4IDYwNjY5IDYwNjcwIDYwNjcxIDYwNjcyIDYwNjczIDYwNjc0IDYwNjc1IDYwNjc2IDYwNjc3IDYwNjc4IDYwNjc5IDYwNjgwIDYwNjgxIDYwNjgyIDYwNjgzIDYwNjg0IDYwNjg1IDYwNjg5IiwicG9ydGZvbGlvcyI6Ijc1MDBHSEMgNzUwMEo0UCBEMzkwMDQgRDQ5MzAwIEcxNDk3NSBHMjMxMDEiLCJzY29wZSI6Ik9yZGVyc1JlYWQgT3JkZXJzQ3JlYXRlIFRyYWRlcyBQZXJzb25hbCBPcmRlcnNSZWFkIE9yZGVyc0NyZWF0ZSBUcmFkZXMgUGVyc29uYWwgT3JkZXJzUmVhZCBPcmRlcnNDcmVhdGUgVHJhZGVzIFBlcnNvbmFsIE9yZGVyc1JlYWQgVHJhZGVzIFBlcnNvbmFsIFRyYWRlcyBPcmRlcnNSZWFkIFRyYWRlcyBQZXJzb25hbCBTdGF0cyIsImV4cCI6MTU4Njc5MDEyNSwiaWF0IjoxNTg2Nzg4MzI1LCJpc3MiOiJBbG9yLklkZW50aXR5IiwiYXVkIjoiQ2xpZW50IFdBUlAgV2FycEFUQ29ubmVjdG9yIn0.pWaKxsWjgjo8BgefolvtXhtWbZexdpgLOFhU9Q0sAEPhjpyKWe-xRFKrHO8HL6g3hiQyZJ72eXcsRGwHjADXrQ",
  refreshToken: "19b1c605-ba0d-4aef-8a72-3b45dd29d62f",
  isTokenNew: false,
  areWeWaitingForTakeProfit: false,
  isItTimeForTrade: false,
  isCheckIfItIsTimeForTradeNeeded: true,
  currentLongMAValue: NaN,
  currentShortMAValue: NaN,
  previousLongMAValue: 0.0,
  previousShortMAValue: 0.0,
  previousLastHistoryTimeframeValue: NaN,
  relativeGainPercents: 0.0,
  trade_parameters:{
    ticker: 'FOOBAR',
    volume: 1
  },
  localHistory: {
    history: [ 
      {}
    ],
    next: 0,
    nodata: true
  },
  lastDeal: {
    orderMarket:{
      "id": 7236256,
      "symbol": "defined",
      "type": "market",
      "side": "foobar",
      "status": "foobar",
      "qty": 1,
      "filledQty": 100,
      "price": 183.29,
      "existing": false,
      "httpCode": -1
    },
    orderLimit:{
      "id": 7236256,
      "symbol": "SBER",
      "type": "market",
      "side": "foobar",
      "status": "foobar",
      "qty": 1,
      "filledQty": 100,
      "price": 183.29,
      "existing": false,
      "httpCode": -1
    },
    orderStopLoss:{
      "id": 326207,
      "symbol": "SBER",
      "type": "takeprofit",
      "stopPrice": 215,
      "side": "foobar",
      "status": "foobar",
      "qty": 1,
      "filledQty": 1,
      "price": 0,
      "existing": false,
      "httpCode": -1
    }
  } 
}

let options = {
  auth: {
    method: 'POST',    
    body: {
      side: 'sell',
      Quantity: 1,
      Instrument: { 
        symbol: 'RTS-9.18', 
        exchange: EXCHANGE // MOEX
      },
      User: { 
        account: '7500GHC', 
        portfolio: '7500GHC' 
      }
    },
    json: true
  },
  marketBuy: {
    method: 'POST',
    
    body: {
      "Quantity": 1,
      "Side": "buy",
      "Instrument": {
        "Symbol": "SBER",
        "Exchange": 'GAME'
      },
      "User": {
        "Account": "L01-00000F00",
        "Portfolio": "D39004"
      },
      "OrderEndUnixTime": 0
    },
    json: true
  },
  marketSell: {
    method: 'POST',
    
    body: {
      "Quantity": 1,
      "Side": "sell",
      "Instrument": {
        "Symbol": "SBER",
        "Exchange": 'GAME'
      },
      "User": {
        "Account": "L01-00000F00",
        "Portfolio": "D39004"
      },
      "OrderEndUnixTime": 0
    },
    json: true
  },
  limitBuy: {
    method: 'POST',
    
    body: {
      "Quantity": 1,
      "Side": "buy",
      "Price": 188.71,
      "Instrument": {
        "Symbol": "SBER",
        "Exchange": 'GAME'
      },
      "User": {
        "Account": "L01-00000F00",
        "Portfolio": "D39004"
      },
      "OrderEndUnixTime": 0
    },
    json: true
  },
  limitSell: {
    method: 'POST',
    
    body: {
      "Quantity": 1,
      "Side": "sell",
      "Price": 188.71,
      "Instrument": {
        "Symbol": "SBER",
        "Exchange": 'GAME'
      },
      "User": {
        "Account": "L01-00000F00",
        "Portfolio": "D39004"
      },
      "OrderEndUnixTime":0
    },
    json: true
  },
  stopLossBuy: {
    method: 'POST',
    
    body: {
      "Quantity": 1,
      "Side": "buy",
      "TriggerPrice": 215,
      "Instrument": {
        "Symbol": "SBER",
        "Exchange": 'GAME'
      },
      "User": {
        "Account": "L01-00000F00",
        "Portfolio": "D39004"
      },
      "OrderEndUnixTime": 0
    },
    json: true
  },
  stopLossSell: {
    method: 'POST',
    
    body: {
      "Quantity": 1,
      "Side": "sell",
      "TriggerPrice": 215,
      "Instrument": {
        "Symbol": "SBER",
        "Exchange": 'GAME'
      },
      "User": {
        "Account": "L01-00000F00",
        "Portfolio": "D39004"
      },
      "OrderEndUnixTime": 0
    },
    json: true
  }    
}

let orderResponse = {
  //unserialisedJSON status 400 Bad Request
  unserialisedJSON: "Provided json can't be properly deserialised, perhaps you made an error or forgot some field",
  //marginLevel status 400 Bad Request
  lowMarginLevel: "Заявка не может быть принята из-за возможного недопустимого снижения уровня маржи.",
  //tooBigPrice status 400 Bad Request
  tooBigPrice: "ОШИБКА: (580) Для выбранного финансового инструмента цена должна быть не больше 184.32",
  //tooLowPrice status 400 Bad Request
  tooLowPrice: "ОШИБКА: (579) Для выбранного финансового инструмента цена должна быть не меньше 177.09",
  //stockExchangeIsClosed status 400 Bad Request
  stockExchangeIsClosed: "Заявка не может быть принята из-за недостаточного встречного спроса/предложения на рынке.",
  //alreadyDeleted status 400 Bad Request
  alreadyDeleted: "ОШИБКА: (916) Заявка не может быть отменена. Указанная заявка уже не активна. Текущий статус заявки 'W'",
  //alreadyActivated status 400 Bad Request
  alreadyActivated: 'Активизированная стоп-заявка не может быть удалена',
  //status 200
  marketBuy: {
    'httpCode': 418,
    "message": "(162) Заявка на покупку N 7455431 зарегистрирована (1 удовлетворено).\r\n",
    "orderNumber": 100500
  },
  marketSell: {
    'httpCode': 418,
    "message": "(163) Заявка на продажу N 7735247 зарегистрирована.\r\n",
    "orderNumber": 100500
  },
  limitBuy: {
    'httpCode': 418,
    "message": "(160) Заявка на покупку N 7520202 зарегистрирована.\r\n",
    "orderNumber": 100500
  },
  limitSell: {
    'httpCode': 418,
    "message": "(163) Заявка на продажу N 7687536 зарегистрирована (1 удовлетворено).\r\n",
    "orderNumber": 100500
  },
  stopLossBuy: {
    'httpCode': 418,
    "message": "Succeeded, OrderNo=326212, EndTime={31.08.18  23:59:59}",
    "orderNumber": 100500
  },
  stopLossSell: {
    'httpCode': 418,
    "message": "Succeeded, OrderNo=326214, EndTime={31.08.18  23:59:59}",
    "orderNumber": 100500
  },
  delete: {
    message: '',
    httpCode: 100500
  }
}

module.exports = {
  API_WARP_URL,
  API_WARPTRANS_URL,
  EXCHANGE,
  DEFAULT_GET_HEADERS,
  DEFAULT_POST_HEADERS,
  DEFAULT_POST_OPTIONS,
  track,
  trade_parameters,
  orderResponse,
  options
}