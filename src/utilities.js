/* здесь хранятся служебные функции*/
const fs = require('fs'); 

let request = require("request");

let {
  trade_parameters,
  DEFAULT_GET_HEADERS
} = require('./parameters'); 

let uuid = require("uuid");

function Utilities() {
  // this.request = _request;
}

Utilities.prototype.sleep = function(ms) {return new Promise(resolve => setTimeout(resolve, ms));}

Utilities.prototype.new_xalorreqid = function() { 
  let x_alor_reqid = uuid.v4().substring(19,30);  
  return x_alor_reqid;
}

Utilities.prototype.new_uuid = function() { return uuid.v4();}

Utilities.prototype.checkTroublesParameters = async function() { /* done */
  
  console.warn("Вставьте свой jwt     токен с сайта в конфигурационный файл parameters.js в объект track в поле track.jwt");
  console.warn("Вставьте свой refresh токен с сайта в конфигурационный файл parameters.js в объект track в поле track.refreshToken");
  console.warn("Робот сможет обновлять токен автоматически, без интерфейса ");

  if ((trade_parameters.toWinPercents < 0) || (trade_parameters.readyToLoosePercents < 0)) {
    console.error('Неверное значение для параметров: toWinPercents или readyToLoosePercents')
    console.error('Значения должны быть больше нуля')
    return false;
    // неправильное значение, надо пытаться заработать больше нуля и рисковать
  }

  if (trade_parameters.readyToLoosePercents + trade_parameters.toWinPercents <= 0.2) {
    console.log('Проверьте значение для суммы параметров: toWinPercents и readyToLoosePercents')
    console.log('Если оно очень мало, то это может приводить к ситуации')
    console.log('в которой как лимитная так и стоплосс заявка успеют исполниться за 1 цикл')
    console.log('Робот логирует такое событие, но он не пытается закрыть образовавшуюся лишнюю позицию, а игнорирует её')
    // маловато, возможно что как лимитная так и стоплосс заявка успеют исполниться за 1 цикл
    // хотя робот и логирует такое событие, он не пытается закрыть лишнюю позицию, а игнорирует её
  }

  if (trade_parameters.qty <= 0 || trade_parameters.qty >= 10000 ) {
    console.error('Неверное значение для параметра: qty')
    console.error('Нельзя торговать 0 акций или больше 9999 акций')
    return false;
    // нельзя торговать 0 акций
    // нельзя торговать очень много акций
  }

  if (trade_parameters.shortMALength >= trade_parameters.longMALength) {
    console.error('Неверное значение для параметра: longMALength или shortMALength')
    console.error('Значения должны быть больше единицы и отличаться')
    return false;
    // нарушает логику индикатора MACD
  }

  if (trade_parameters.shortMALength <= 1 || trade_parameters.longMALength <= 1) {
    console.error('Неверное значение для параметра: longMALength или shortMALength')
    console.error('Значения должны быть больше единицы и отличаться')
    return false;
    // 
  }

  if (trade_parameters.shortMALength > 25 || trade_parameters.longMALength > 25) {
    console.error('Неверное значение для параметра: longMALength или shortMALength')
    console.error('Значения должны быть не больше 25 и отличаться')
    return false;
    // 
  }

  if (trade_parameters.exponentialDamper > 0.5) {
    console.error('Неверное значение параметра: exponentialDamper')
    console.error('При больших значениях слишком сильное подавление влияния предыдущих цен, осциллятор не будет работать правильно')
    return false;
    // слишком сильное подавление влияния предыдущих цен, осциллятор не будет работать правильно
  }

  if (trade_parameters.exponentialDamper < -0.05) {
    console.error('Неверное значение параметра: exponentialDamper')
    console.error('При отрицательных значениях заметное усиление влияния предыдущих цен, осциллятор не будет работать правильно')
    return false;
    // заметное усиление влияния предыдущих цен, осциллятор не будет работать правильно
  }

  if ((parseInt(trade_parameters.hhmmEnd.substring( 0,2),10) * 60 + parseInt(trade_parameters.hhmmEnd.substring( 3,5),10))
  <=(parseInt(trade_parameters.hhmmStart.substring(0,2),10) * 60 + parseInt(trade_parameters.hhmmStart.substring(3,5),10))) {
    console.error('Неверное значение параметров: hhmmStart и hhmmEnd')
    console.error('Нельзя задать торговый интервал отрицательной длины')
    return false;
    // нельзя задать торговый интервал отрицательной длины
  }

  if (trade_parameters.deltaMacdRub <= 0) {
    console.error('Неверное значение параметра: deltaMacdRub')
    console.error('Значение должно быть положительным')
    return false;
    /* может нарушать логику индикатора MACD */
  }

  if (trade_parameters.mainLoopDelay <= 50) {
    console.error('Неверное значение параметра: mainLoopDelay')
    console.error('Нельзя слать запросы на сервер очень часто ')
    return false;
    /* я не знаю сколько миллисекунд тут должно быть для ограничения, но я знаю что нельзя устраивать dos */
  }

  if (trade_parameters.smalldelay <= 1) {
    console.error('Неверное значение параметра: smalldelay')
    console.error('Нельзя выводить сообщения с задержкой менее 1 миллисекунды')
    return false;
    /* нельзя выводить сообщения с задержкой менее 1 миллисекунды*/
  }

  if (trade_parameters.waitMarketFillMax < 1) {
    console.error('Неверное значение параметра: waitMarketFillMax')
    console.error('Нужно сделать хотя бы одну попытку удостовериться в исполнении заявки')
    return false;
    /*нужно сделать хотя бы одну попытку удостовериться в том, что заявка исполнилась*/
  }
  return true;
}

Utilities.prototype.readConfiguration = function(track) {
  let isConfigurationCorrect = false;
  let configurationFile=fs.readFileSync(process.argv[2]);
  
  if (configurationFile === undefined) {
    console.log('Конфигурационный файл не найден: ' + process.argv[2]);
  } else {
    let configurationFileJSON = JSON.parse(configurationFile);
    
    if (configurationFileJSON.hasOwnProperty('ticker') !== true) {
      console.log('Конфигурационный файл не содержит свойства ticker: ' + process.argv[2]);
    } else if (configurationFileJSON.ticker.toUpperCase() !== 'SBER') {
      console.log('Конфигурационный файл содержит некорректное значение свойства ticker: ' + configurationFileJSON.ticker);
    } else {
    
      if (configurationFileJSON.hasOwnProperty('volume') !== true) {
        console.log('Конфигурационный файл не содержит свойства volume: ' + process.argv[2]);
      } else if (typeof Math.floor(configurationFileJSON.volume) !== 'number') {
        console.log('Конфигурационный файл содержит некорректное значение свойства volume: ' + configurationFileJSON.volume + ' ' + (typeof configurationFileJSON.volume));
      } else if (configurationFileJSON.volume <= 0 || configurationFileJSON.volume >= 100) {
        console.log('Конфигурационный файл содержит некорректное значение свойства volume: ' + configurationFileJSON.volume);
      } else {
        isConfigurationCorrect = true;
        track.trade_parameters.ticker = (configurationFileJSON.ticker).toUpperCase();
        track.trade_parameters.volume = Math.floor(configurationFileJSON.volume);
      }
    }    
  }
  return isConfigurationCorrect;
}

Utilities.prototype.callTime = function(jwt) {
  const timerequest_options = {
      method: 'GET',
      url: 'https://apidev.alor.ru/md/time',
      headers: {
        ...DEFAULT_GET_HEADERS,
        "Authorization": `Bearer ${jwt}`,
      } 
  }
  return new Promise(function(resolve, reject) {
    request(timerequest_options, function (error, response, body) {
      if (error) throw new Error(error);
      resolve(body);
    });
  });
}

Utilities.prototype.httpBuildQuery = function(query) {
  return '?' + Object.keys(query).map(key => `${key}=${query[key]}`).join('&');
}

module.exports = {
  Utilities
}