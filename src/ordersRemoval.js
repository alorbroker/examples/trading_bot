/*здесь хранятся функции для удаления заявок*/
let request = require("request");

let {
  // track,
  // trade_parameters,
  orderResponse,
  // options,
  // API_WARP_URL,
  API_WARPTRANS_URL,
  // EXCHANGE,
  // DEFAULT_GET_HEADERS,
  DEFAULT_POST_HEADERS
} = require('./parameters'); 

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

function OrdersRemoval(){
}

OrdersRemoval.prototype.deleteActionsLimitOrMarket = function(options_ordersDelete) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_ordersDelete, function (error, response, body) {
      if (error) throw new Error(error);
      const delete_response = {}//здесь body приходит как object
      delete_response.message = body;//здесь body приходит как object
      delete_response.httpCode = response.statusCode;
      resolve(delete_response);
    });
  });
}

OrdersRemoval.prototype.removeLimitOrderBuyAsync = async function(orderNumber,jwt) { /* done */
  const query = {
    portfolio: "D39004",
    stop: false
  };

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/${orderNumber.toString()}${utilities.httpBuildQuery(query)}`; 

  const orderDeleteOptions = {
    method: 'DELETE',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url
  };

  for (let tryagain = 0; tryagain < 5; tryagain++) {
    orderResponse.delete = await this.deleteActionsLimitOrMarket(orderDeleteOptions);
    await utilities.sleep(100);
  }

  if (orderResponse.delete.message !== 'Succeeded' && orderResponse.delete.message !== orderResponse.alreadyDeleted) {
    console.log('Не могу удалить лимитную заявку. ' + orderNumber + ";status " , orderResponse.delete.httpCode)
  }
  return orderResponse.delete;
}

OrdersRemoval.prototype.removeLimitOrderSellAsync = async function(orderNumber,jwt) { /* done */  
  const query = {
    portfolio: "D39004",
    stop: false
  };

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/${orderNumber.toString()}${utilities.httpBuildQuery(query)}`; 

  const orderDeleteOptions = {
    method: 'DELETE',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url
  };

  for (let tryagain = 0; tryagain < 5; tryagain++) {
    orderResponse.delete = await this.deleteActionsLimitOrMarket(orderDeleteOptions);
    await utilities.sleep(100);
  }
  
  if (orderResponse.delete.message !== 'Succeeded' && orderResponse.delete.message !== orderResponse.alreadyDeleted) {
    console.log('Не могу удалить лимитную заявку. ' + orderNumber + ";status " , orderResponse.delete.httpCode)
  }
  return orderResponse.delete;
}

OrdersRemoval.prototype.deleteActionsStopLoss = function(options_stopOrdersDelete) { /* done */
  return new Promise(function(resolve, reject) {
    request(options_stopOrdersDelete, function (error, response, body) {
      if (error) throw new Error(error);
      const delete_response = {}//здесь body приходит как object
      delete_response.message = body;//здесь body приходит как object
      delete_response.httpCode = response.statusCode;
      resolve(delete_response);
    });
  });
}

OrdersRemoval.prototype.removeStopLossOrderBuyAsync = async function(stopOrderNumber,jwt) { /* done */
  const query = {
    portfolio: "D39004",
    stop: true
  };

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/${stopOrderNumber.toString()}${utilities.httpBuildQuery(query)}`; 

  const stopOrderDeleteOptions = {
    method: 'DELETE',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url
  };

  for (let tryagain = 0; tryagain < 5; tryagain++) {
    orderResponse.delete = await this.deleteActionsStopLoss(stopOrderDeleteOptions);
    await utilities.sleep(100);
  }
  
  if (orderResponse.delete.message === orderResponse.alreadyActivated) {
    console.log('Не могу удалить стоплосс заявку. ' + stopOrderNumber + ", т.к. она уже активировалась")
  } else if (orderResponse.delete.message !== 'Succeeded') {
    console.log('Не могу удалить стоплосс заявку. ' + stopOrderNumber + ";status " , orderResponse.delete.httpCode)
  }
  return orderResponse.delete;
}

OrdersRemoval.prototype.removeStopLossOrderSellAsync = async function(stopOrderNumber,jwt) { /* done */
  const query = {
    portfolio: "D39004",
    stop: true
  };

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/${stopOrderNumber.toString()}${utilities.httpBuildQuery(query)}`; 

  const stopOrderDeleteOptions = {
    method: 'DELETE',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url
  };

  for (let tryagain = 1; tryagain < 5; tryagain++) {  
    orderResponse.delete = await this.deleteActionsStopLoss(stopOrderDeleteOptions);
    await utilities.sleep(100);
  }

  if (orderResponse.delete.message === orderResponse.alreadyActivated) {
    console.log('Не могу удалить стоплосс заявку. ' + stopOrderNumber + ", т.к. она уже активировалась")
  } else if (orderResponse.delete.message !== 'Succeeded') {
    console.log('Не могу удалить стоплосс заявку. ' + stopOrderNumber + ";status " , orderResponse.delete.httpCode)
  }
  return orderResponse.delete;
}

OrdersRemoval.prototype.removeMarketOrderBuyAsync = async function(orderNumber,jwt) {
  const query = {
    portfolio: "D39004",
    stop: false
  };

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/${orderNumber.toString()}${utilities.httpBuildQuery(query)}`; 

  const orderDeleteOptions = {
    method: 'DELETE',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url
  };
  
  orderResponse.delete = await this.deleteActionsLimitOrMarket(orderDeleteOptions);
  await utilities.sleep(100);
  
  if (orderResponse.delete !== 'Succeeded') {
    console.log('Не могу удалить рыночную заявку. ' + orderNumber + ";status " , orderResponse.delete)
  } else {
    console.log('Рыночная заявка отозвана')
  }
  return orderResponse.delete;
}

OrdersRemoval.prototype.removeMarketOrderSellAsync = async function(orderNumber,jwt) {
  const query = {
    portfolio: "D39004",
    stop: false
  };

  const url = `${API_WARPTRANS_URL}/TRADE/v2/client/orders/${orderNumber.toString()}${utilities.httpBuildQuery(query)}`; 

  const orderDeleteOptions = {
    method: 'DELETE',
    headers: {
      ...DEFAULT_POST_HEADERS,
      "Authorization": `Bearer ${jwt}`,
    },
    url
  };
  
  orderResponse.delete = await this.deleteActionsLimitOrMarket(orderDeleteOptions);
  await utilities.sleep(100);
  
  if (orderResponse.delete !== 'Succeeded') {
    console.log('Не могу удалить рыночную заявку. ' + orderNumber + ";status ",orderResponse.delete)
  } else {
    console.log('Рыночная заявка отозвана')
  }
  return orderResponse.delete;
}

module.exports = {
  // // this.deleteActionsLimitOrMarket, 
  // removeLimitOrderBuyAsync, 
  // removeLimitOrderSellAsync, 
  // // this.deleteActionsStopLoss, 
  // removeStopLossOrderBuyAsync, 
  // removeStopLossOrderSellAsync, 
  // removeMarketOrderBuyAsync, 
  // removeMarketOrderSellAsync
  OrdersRemoval
}

