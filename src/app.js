#!/usr/bin/env node

let {
  track, 
  trade_parameters
} = require('./parameters'); 

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

let {
  // procedures.checkIfItIsTimeForTrade(),
  // procedures.elementaryTradeStep()
  Procedures
} = require('./procedures');

const procedures = new Procedures();

let {
  Tokens
} = require('./tokens');

const tokens = new Tokens();

let {
  subscribeToWebSocket  
} = require('./webSocket');

eternalRecurrence();

async function eternalRecurrence () {
  let currentServerTime;
  let main_loop = 0;
  console.log('Торговый робот запущен.')
  console.log('Робот пытается принести прибыль, но не гарантирует прибыли.')
  
  if (await utilities.checkTroublesParameters() !== true || await utilities.readConfiguration(track) !== true) {
    return; // checkTroublesParameters выводит причину остановки в консоль, readConfiguration считывает конфиг
  }
  await tokens.setTokenAsync(track); // нужна авторизация для времени
  currentServerTime = await utilities.callTime(track.jwt);
  track.startingTime = currentServerTime;
  if ((new Date(1000 * currentServerTime)).toString() === "Invalid Date") {
    console.error("Не удалось подключиться к серверу "+ currentServerTime);
    return;
  }
  console.log('Робот установил связь с сервером: ' + (new Date(1000 * currentServerTime)).toString().substring(4,25));
  
  //subscribeToWebSocket(track); 
  //console.log(track.test)
  track.isItTimeForTrade = true;
  track.isCheckIfItIsTimeForTradeNeeded = true;
  track.isWsOpen = false;
  while (true) {
    ++main_loop;
    
    if (track.isCheckIfItIsTimeForTradeNeeded === true ) { 
      await procedures.checkIfItIsTimeForTrade(track);
      track.isCheckIfItIsTimeForTradeNeeded = false;
      if (track.isItTimeForTrade === true) { 
        await tokens.setTokenAsync(track);
        if (track.isWsOpen === false) {
          subscribeToWebSocket(track); 
        }
      }
    }
    
    if (track.isItTimeForTrade === true) { 
      //console.log('Are we trading yet ?', track.jwt.toString().substring(0,8));
      await procedures.elementaryTradeStep(track); 
    } else {
      track.lastDeal.orderMarket.status = 'foobar';
      track.lastDeal.orderLimit.status = 'foobar';
      track.lastDeal.orderStopLoss.status = 'foobar'; 
      track.areWeWaitingForTakeProfit = false; 
      /* если попытаться проверить вчерашние заявки, то будет 500, поэтому сбрасываем статус*/
    } 
    
    if (track.localHistory.history.length >= 1) { // если есть история, то надо запомнить время последнего таймфрейма
      track.previousLastHistoryTimeframeValue = track.localHistory.history[track.localHistory.history.length-1].time ;
    }

    if (main_loop%50 === 0 ) { 
      /* 
      Пора заново проверить время. Торговый интервал (см параметры) мог завершиться. 
      Это нужно для того, чтобы заранее отсечь проблемные(для MACD) участки открытия и закрытия торговой сессии
      */  
      console.log('Ожидание нового цикла. Текущая прибыль(убыток) в процентах      : ' + track.relativeGainPercents);
      if (track.isWsOpen === true) {
        console.log('Объём истории ws составляет: ' + track.wsHistory.length + `. Текущая позиция по инструменту ${track.trade_parameters.ticker} : ${track.positions.qty}`);
      }
      track.isCheckIfItIsTimeForTradeNeeded = true; 
      track.isItTimeForTrade = false;
      if (main_loop === 50*10000 ){
        main_loop = 0; 
      }
      
    } 
        
    await utilities.sleep(trade_parameters.mainLoopDelay);
  }
  console.log('Робот завершил работу ');
}