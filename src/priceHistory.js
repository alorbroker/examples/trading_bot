/*здесь хранятся функции для получения и обработки истории*/

const { 
  trade_parameters,
} = require('./parameters'); 

let {
  Utilities
} = require('./utilities');

const utilities = new Utilities();

function History(_request) {
  this.request = _request;
}

History.prototype.obtainHistoryAsync = async function(track) { /* done */

  let localHistory = {
    history: [],
    next: 0,
    nodata: true
  };

  for(waitForWebSocket = 0; waitForWebSocket < trade_parameters.waitForWebSocketTries; waitForWebSocket++) {
    
    if (track.wsHistory.length === 0) {
      await utilities.sleep(500);
    }
  }

  if (track.wsHistory.length === 0) {
    console.log( `Не удалось получить историю. История свечей из Websocket пуста. Вероятно торговая сессия закрыта.`);
    track.isCheckIfItIsTimeForTradeNeeded = true;    
  } else {
    for (let i = track.wsHistory.length - trade_parameters.longMALength; i < track.wsHistory.length; i++) {
      localHistory.history[i - track.wsHistory.length + trade_parameters.longMALength] = (track.wsHistory[i]);
    }
  }
  return localHistory;
}

module.exports = History;
